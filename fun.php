<?php

	$myCss = getCss();
	$myJS  = getjQuery();

	function getCss()
	{
		$style = file("style.css");
		$style = implode($style);
		$style = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '),"", $style);
		$style = explode("}", $style);
		$style = array_filter($style);

		return $style;
	}

	function getjQuery()
	{
		$jQuery  = getCss();
		$jQarray = array();

		foreach ($jQuery as $value) {

			$value  = explode("{", $value);

			$jQclass = trim($value[0]);

			$jQcss = $value[1];
			$jQcss = str_replace(";", "','", $jQcss );
			$jQcss = "'" . str_replace(":", "':'", $jQcss );
			$jQcss = substr($jQcss  , 0, -2);
			
			$jQarray[] = "<b>$('" . $jQclass .  "').css({</b> " .  $jQcss . " <b>});</b>" ;
		}

		return $jQarray;
	}

	function echojQuery()
	{
		$jQcss = implode(getjQuery());
		$jQcss = strip_tags($jQcss);

		echo $jQcss;
	}

	require __DIR__.'/vendor/autoload.php';

	$inline = $_POST['name'];
	$inline = preg_replace('~<script(.*)/script>~Uis', '', $inline);
	$inline = Mihaeu\HtmlFormatter::format($inline);

	file_put_contents('inline.html', $inline);
